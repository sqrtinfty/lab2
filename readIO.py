import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)
GPIO.setup(23,GPIO.IN)
print("GPIO pin 23 set as input")

count=0
oldValue=-1

while True:
      inputValue=GPIO.input(23)
      if(inputValue!=oldValue):
          print("GPIO 23 changed to " + str(inputValue))
          oldValue=inputValue
          time.sleep(.01)
