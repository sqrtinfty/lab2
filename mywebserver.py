from flask import Flask
import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(18,GPIO.IN)
GPIO.setup(23,GPIO.IN)
GPIO.setup(24,GPIO.IN)
app=Flask(__name__)


@app.route('/')
def hello_world():
    return "Hello World!"
@app.route('/date/')
def datePage():
    return time.ctime()
@app.route('/switches/')
def switchesPage():
    value1=GPIO.input(18)
    value2=GPIO.input(23)
    value3=GPIO.input(24)
    return "GPIO18: %d, GPIO23 %d, GPIO24 %d" % (value1, value2, value3)


if __name__ =='__main__':
    app.run(host='0.0.0.0')
