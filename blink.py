import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setup(17,GPIO.OUT)
GPIO.setup(22,GPIO.OUT)
GPIO.setup(27,GPIO.OUT)
GPIO.setup(18,GPIO.IN)
GPIO.setup(23,GPIO.IN)
GPIO.setup(24,GPIO.IN)

value1=0
value2=0
value3=0

while True:

      value1=GPIO.input(18)
      value2=GPIO.input(23)
      value3=GPIO.input(24)

      if(value1==1):
            GPIO.output(17,GPIO.HIGH)
      else:
            GPIO.output(17,GPIO.LOW)
      if(value2==1):
            GPIO.output(22,GPIO.HIGH)
      else:
            GPIO.output(22,GPIO.LOW)
      if(value3==1):
            GPIO.output(27,GPIO.HIGH)
      else:
            GPIO.output(27,GPIO.LOW) 

      if(value1==value2==value3):
            GPIO.output(17,GPIO.HIGH)
            time.sleep(0.2)
            GPIO.output(17,GPIO.LOW)
            time.sleep(0.2)
            GPIO.output(22,GPIO.HIGH)
            time.sleep(0.2)
            GPIO.output(22,GPIO.LOW)
            time.sleep(0.2)
            GPIO.output(27,GPIO.HIGH)
            time.sleep(0.2)
            GPIO.output(27,GPIO.LOW)
            time.sleep(0.2)

